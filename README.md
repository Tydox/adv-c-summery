# C Summery Book by Danno
[TOC]
### Table of Context

1. General Keywords
2. Union
3. Linked Lists
   1. Node
4. Stack
5. ADT
6. Queue
7. API










# Extras
### Useful Videos:

2. [^Singly-Linked Lists]: https://www.youtube.com/watch?v=zQI3FyWm144

3. [^Pointers]: https://www.youtube.com/watch?v=XISnO2YhnsY

4. 

5. 

6. 

   

### Useful Books:

1. [^c traps and pitfalls]: http://www.literateprogramming.com/ctraps.pdf

2. [^K&R C Programming Language 2nd Edition]: http://www2.cs.uregina.ca/~hilder/cs833/Other%20Reference%20Materials/The%20C%20Programming%20Language.pdf

3. [^King's C Programming  A Modern Approach]: https://programmer-books.com/wp-content/uploads/2018/05/C%20Programming.pdf

4. [^H&S C99 - C A Reference Manual Fifth Edition]: https://savedparadigms.files.wordpress.com/2014/09/harbison-s-p-steele-g-l-c-a-reference-manual-5th-ed.pdf

5. [^Pointers On C]: https://epdf.pub/download/pointers-on-cbc61b477640c677f988932b8db643fdf41590.html

   


### General Keywords - מילות נפוצות

1. Stack - Static\Local Memory Allocation

2. Heap - Dynamic Memory Allocation
3. 
4. 
5. 







### Template

##### Introduction:

**Simple** : 

> 

**Advanced**: 

> 

##### Important Key-Points:

> 

##### Do's & Dont's

| ✅ Do this | ❌ Don't do this |
| :-------: | :-------------: |
|           |                 |

##### Example:

```c
<code>
```















# Topics



### Union

##### **Introduction** 

**Simple** :

> A Union is a structure where you have *only 1 of the variables active* at the same time, meaning *we can always have 1 active object.*

**Advanced**: 

> A **union** is a special data type available in C that allows to  store different data types in the same memory location. You can define a union with many members, but only one member can contain a value at any given time. Unions provide an efficient way of using the same memory  location for multiple-purpose.

##### Important Key-Points

> This is useful when we want to create a structure where the body includes a few variables but we only need to access one of them at a specific moment.

##### Do's & Dont's

|                 ✅ Do this                  |                ❌ Don't do this                |
| :----------------------------------------: | :-------------------------------------------: |
| Remember you can only have 1 object active | Forgot and try to save 2 datas into the union |

##### Examples

Stock:

```c
union [union tag] {
   member definition;
   member definition;
   ...
   member definition;
} [one or more union variables];  
```

Template for me:

```c
typedef union {
   member definition;
   member definition;
   ...
   member definition;
}[name of union i will want to use as]

Main -
    name [variable];
```

```c
typedef union {
	int x;
}nums;

main(){
    	nums list;
}
```





### Linked Lists

##### Introduction:

**Simple** : 

> A linked It is a series of connected "nodes" that contains the "address" of the  next node. Each node can store a data point which may be a number, a  string or any other type of data. 
>
> Video: [^Singly-Linked Lists]

**Advanced**: 

> There are three common types of Linked List.
>
> 1. Singly Linked List
> 2. Doubly Linked List
> 3. Circular Linked List

##### Singly Linked List

> It is the most common. Each node has data and a pointer to the next node.
>
> ##### Node is represented as:
>
> ```c
> struct node {
>     int data; // some sort of data to keep inside
>     struct node *next; // pointer to next node
> }
> ```
>
> 
>
> ![](C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\linked-list-concept_0.png)



#### Doubly Linked List

> We add a pointer to the previous node in a doubly-linked list. Thus, we can go in either direction: forward or backward.
>
> ##### Node is represented as:
>
> ```c
> struct node {
>     int data;
>     struct node *next;
>     struct node *prev;
> }
> ```
>
> ![](C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\doubly-linked-list-concept.png)



#### Circular Linked List

> A circular linked list is a variation of a linked list in which the last element is linked to the first element. This forms a circular loop.
>
> A circular linked list can be either singly linked or doubly linked.
>
> - for singly linked list, next pointer of last item points to the first item
> - In the doubly linked list, prev pointer of the first item points to the last item as well.
>
> A three-member circular singly linked list can be created as:
>
> ![](C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\circular-linked-list.png)

##### Important Key-Points:

> **Singly linked list:**
>
> ```c
> /* Initialize nodes */
> struct node *head;
> struct node *one = NULL;
> struct node *two = NULL;
> struct node *three = NULL;
> 
> /* Allocate memory */
> one = malloc(sizeof(struct node));
> two = malloc(sizeof(struct node));
> three = malloc(sizeof(struct node));
> 
> /* Assign data values */
> one->data = 1;
> two->data = 2;
> three->data = 3;
> 
> /* Connect nodes */
> one->next = two;
> two->next = three;
> three->next = NULL;
> 
> /* Save address of first node in head */
> head = one;
> ```
>
> **Doubly linked list :**
>
> ```C
> /* Initialize nodes */
> struct node *head;
> struct node *one = NULL;
> struct node *two = NULL;
> struct node *three = NULL;
> 
> /* Allocate memory */
> one = malloc(sizeof(struct node));
> two = malloc(sizeof(struct node));
> three = malloc(sizeof(struct node));
> 
> /* Assign data values */
> one->data = 1;
> two->data = 2;
> three->data = 3;
> 
> /* Connect nodes */
> one->next = two;
> one->prev = NULL;
> 
> two->next = three;
> two->prev = one;
> 
> three->next = NULL;
> three->prev = two;
> 
> /* Save address of first node in head */
> head = one;
> 
> ```
>
> **Circular singly linked list :**
>
> ```	
> /* Initialize nodes */
> struct node *head;
> struct node *one = NULL;
> struct node *two = NULL;
> struct node *three = NULL;
> 
> /* Allocate memory */
> one = malloc(sizeof(struct node));
> two = malloc(sizeof(struct node));
> three = malloc(sizeof(struct node));
> 
> /* Assign data values */
> one->data = 1;
> two->data = 2;
> three->data = 3;
> 
> /* Connect nodes */
> one->next = two;
> two->next = three;
> three->next = one;
> 
> /* Save address of first node in head */
> head = one;
> ```
>
> 



##### Do's & Dont's

| ✅ Do this | ❌ Don't do this |
| :-------: | :-------------: |
|           |                 |

##### Example:

```c
// Linked list operations in C

#include <stdio.h>
#include <stdlib.h>

// Create a node
struct Node {
  int data;
  struct Node* next;
};

void insertAtBeginning(struct Node** head_ref, int new_data) {
  // Allocate memory to a node
  struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));

  // insert the data
  new_node->data = new_data;

  new_node->next = (*head_ref);

  // Move head to new node
  (*head_ref) = new_node;
}

// Insert a node after a node
void insertAfter(struct Node* prev_node, int new_data) {
  if (prev_node == NULL) {
    printf("the given previous node cannot be NULL");
    return;
  }

  struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
  new_node->data = new_data;
  new_node->next = prev_node->next;
  prev_node->next = new_node;
}

void insertAtEnd(struct Node** head_ref, int new_data) {
  struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
  struct Node* last = *head_ref; /* used in step 5*/

  new_node->data = new_data;
  new_node->next = NULL;

  if (*head_ref == NULL) {
    *head_ref = new_node;
    return;
  }

  while (last->next != NULL)
    last = last->next;

  last->next = new_node;
  return;
}

void deleteNode(struct Node** head_ref, int key) {
  struct Node *temp = *head_ref, *prev;

  if (temp != NULL && temp->data == key) {
    *head_ref = temp->next;
    free(temp);
    return;
  }
  // Find the key to be deleted
  while (temp != NULL && temp->data != key) {
    prev = temp;
    temp = temp->next;
  }

  // If the key is not present
  if (temp == NULL) return;

  // Remove the node
  prev->next = temp->next;

  free(temp);
}

// Print the linked list
void printList(struct Node* node) {
  while (node != NULL) {
    printf(" %d ", node->data);
    node = node->next;
  }
}

// Driver program
int main() {
  struct Node* head = NULL;

  insertAtEnd(&head, 1);
  insertAtBeginning(&head, 2);
  insertAtBeginning(&head, 3);
  insertAtEnd(&head, 4);
  insertAfter(head->next, 5);

  printf("Linked list: ");
  printList(head);

  printf("\nAfter deleting an element: ");
  deleteNode(&head, 3);
  printList(head);
}
```





##### Linked List Operations: Traverse, Insert and Delete

> Two important points to remember:
>
> - head points to the first node of the linked list
> - next pointer of the last node is NULL, so if the next current node is NULL, we have reached the end of the linked list.
>
> In all of the examples, we will assume that the linked list has three nodes `1 --->2 --->3` with node structure as below:

```c
struct node
{
  int data;
  struct node *next;
};
```



##### How to Traverse a Linked List - [Travel\Skim]

> Displaying the contents of a linked list is very simple. We keep moving the temp node to the next one and display its contents.
>
> When temp is NULL, we know that we have reached the end of the linked list so we get out of the while loop.

```c
struct node *temp = head;
printf("\n\nList elements are - \n");
while(temp != NULL)
{
     printf("%d --->",temp->data);
     temp = temp->next;
}
```

The output of this program will be:

```
List elements are - 
1 --->2 --->3 --->
```



##### How to Add Elements to a Linked List                 

> You can add elements to either the beginning, middle or end of the linked list.

> **Add to the beginning**
>
> - Allocate memory for new node
> - Store data
> - Change next of new node to point to head
> - Change head to point to recently created node

```c
struct node *newNode;
newNode = malloc(sizeof(struct node));
newNode->data = 4;
newNode->next = head;
head = newNode;
```

> ##### Add to the End
>
> - Allocate memory for new node
> - Store data
> - Traverse to last node
> - Change next of last node to recently created node

```c
struct node *newNode;
newNode = malloc(sizeof(struct node));
newNode->data = 4;
newNode->next = NULL;

struct node *temp = head;
while(temp->next != NULL){
  temp = temp->next;
}

temp->next = newNode;
```

##### Add to the Middle

> - Allocate memory and store data for new node
> - Traverse to node just before the required position of new node
> - Change next pointers to include new node in between

```c
struct node *newNode;
newNode = malloc(sizeof(struct node));
newNode->data = 4;

struct node *temp = head;

for(int i=2; i < position; i++) {
    if(temp->next != NULL) {
        temp = temp->next;
    }
}
newNode->next = temp->next;
temp->next = newNode;
```



##### How to Delete from a Linked List?

You can delete either from beginning, end or from a particular position.



##### Delete from beginning

> Point head to the second node
>
> ```c
> head = head->next;
> ```
>
> ##### 



##### Delete from end

> - Traverse to second last element
> - Change its next pointer to null
>
> ```c
> struct node* temp = head;
> while(temp->next->next!=NULL){
>   temp = temp->next;
> }
> temp->next = NULL;
> ```
>
> 



##### Delete from middle

> - Traverse to element before the element to be deleted
> - Change next pointers to exclude the node from the chain
>
> ```c
> for(int i=2; i< position; i++) {
>     if(temp->next!=NULL) {
>         temp = temp->next;
>     }
> }
> 
> temp->next = temp->next->next;
> ```





### Queue - תור

##### Introduction:

**Simple** : 

> A queue is a useful data structure in programming. It is similar to the ticket queue outside a cinema hall.
>
> Where the **first person** entering the queue (waiting line) is the **first person** who gets the ticket. 
>
> Queue follows the **First In First Out(FIFO)** rule - the item that goes in first is the item that comes out first too.

**Advanced**: 

> In the image below, since 1 was kept in the queue before 2, it was  the first to be removed from the queue as well. It follows the **FIFO** rule.
>
> In programming terms, putting an item in the queue is called an  "enqueue" and removing an item from the queue is called "dequeue".



![Queue](C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\queue.png)

##### Important Key-Points: 

> ### Queue Specifications
>
> A queue is an object or more specifically an abstract data structure(ADT) that allows the following operations:
>
> - **EnQueue**: Add an element to the end of the queue
> - **DeQueue**: Remove an element from the front of the queue
> - **IsEmpty**: Check if the queue is empty
> - **IsFull**: Check if the queue is full
> - **Peek**: Get the value of the front of the queue without removing it
>
> ```c
> void print(int* array, int front, int tail);
> void enqueue(int* array, int* front, int* tail, int item);
> int dequeue(int* array, int*front, int* tail);```
> ```

> ### How Queue Works
>
> Queue operations work as follows:
>
> 1. Two pointers called **FRONT (HEAD)** and **REAR (TAIL)** are used to keep track of the first and last elements in the queue.
> 2. When **initializing** the **queue**, we set the value of FRONT and REAR to -1.
> 3. On **enqueuing** an element, we increase the value of REAR index and place the new element in the position pointed to by REAR.
> 4. On **dequeuing** an element, we return the value pointed to by FRONT and increase the FRONT index.
> 5. Before **enqueuing**, we check if the queue is already full.
> 6. Before **dequeuing**, we check if the queue is already empty.
> 7. When **enqueuing** the first element, we set the value of FRONT to 0.
> 8. When **dequeuing** the last element, we reset the values of FRONT and REAR to -1.

> ### How Queue Works
>
> | Queue operations work as follows:                            |
> | :----------------------------------------------------------- |
> | 1. Two pointers called **FRONT (HEAD)** and **REAR (TAIL)** are used to keep track of the first and last elements in the queue. |
> | 2. When **initializing** the **queue**, we set the value of FRONT and REAR to -1. |
> | 3. On **enqueuing** an element, we increase the value of REAR index and place the new element in the position pointed to by REAR. |
> | 4. On **dequeuing** an element, we return the value pointed to by FRONT and increase the FRONT index. |
> | 5. Before **enqueuing**, we check if the queue is already full. |
> | 6. Before **dequeuing**, we check if the queue is already empty. |
> | 7. When **enqueuing** the first element, we set the value of FRONT to 0. |
> | 8.    When **dequeuing** the last element, we reset the values of FRONT and REAR to -1. |
>
> <img src="C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\Queue-program-enqueue-dequeue.png" alt="Visualize" style="zoom: 50%;" />







##### Do's & Dont's

|                  ✅ Do this                  |                       ❌ Don't do this                        |
| :-----------------------------------------: | :----------------------------------------------------------: |
| Always check if you received an empty queue | Run without knowing if there is anything in queue, potentially fucking everything up |

##### Example:

```c
#include <stdio.h>
#define SIZE 5

void enQueue(int);
void deQueue();
void display();

int items[SIZE], front = -1, rear = -1;

int main() {
  //deQueue is not possible on empty queue
  deQueue();

  //enQueue 5 elements
  enQueue(1);
  enQueue(2);
  enQueue(3);
  enQueue(4);
  enQueue(5);

  //6th element can't be added to queue because queue is full
  enQueue(6);

  display();

  //deQueue removes element entered first i.e. 1
  deQueue();

  //Now we have just 4 elements
  display();

  return 0;
}

void enQueue(int value) {
  if (rear == SIZE - 1)
    printf("\nQueue is Full!!");
  else {
    if (front == -1)
      front = 0;
    rear++;
    items[rear] = value;
    printf("\nInserted -> %d", value);
  }
}

void deQueue() {
  if (front == -1)
    printf("\nQueue is Empty!!");
  else {
    printf("\nDeleted : %d", items[front]);
    front++;
    if (front > rear)
      front = rear = -1;
  }
}

// Function to print the queue
void display() {
  if (rear == -1)
    printf("\nQueue is Empty!!!");
  else {
    int i;
    printf("\nQueue elements are:\n");
    for (i = front; i <= rear; i++)
      printf("%d  ", items[i]);
  }
  printf("\n");
}
```

##### Limitation

> As you can see in the image below, after a bit of enqueuing and dequeuing, the size of the queue has been reduced.
>
> <img src="C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\why-circular-queue_0.png" alt="circular queue" style="zoom:67%;" />
>
> The indexes 0 and 1 can only be used after the queue is reset when all the elements have been dequeued.
>
> After REAR reaches the last index, if we can store extra  elements in the empty spaces (0 and 1), we can make use of the empty  spaces. This is implemented by a modified queue called circular queue.

##### Queue Applications

> - CPU scheduling, Disk Scheduling
> - When data is transferred asynchronously between two processes.Queue is used for synchronization. eg: IO Buffers, pipes, file IO, etc
> - Handling of interrupts in real-time systems.
> - Call Center phone systems uses Queues to hold people calling them in an order



Queue - תור

queue is ADT

last entered object is last in line and oldest entered is first in line.

 Meaning:

**First In First Out**

**Last In Last Out**



Front\ **Head** - first in queue

Back\Read\ **Tail** - last in queue



When working with Queue you need 2 pointers to the start and end of queue

enqueue - insert a new object

dequeue - remove an old object



Some extra functions:

check if queue is empty

check amout of objects in queue

check the first object in queue is equal to some value



Working with Queue & Arrays

We need to have 2 indexs: pointer to head and pointer to tail

When we start with an empty list we will set head & tail to be = -1

```c
int tail =-1

int head=-1
```

Dequeue:

special occasion

if front == tail that means there is only 1 object  and if we deque it then we need to set both to =-1

else we just take the front +1

It is like a slinky, when we deque we remove from the bottom of the list but when we enque we add to the end of the list

To check if queue is empty:

```c
if(front== -1)
{
    printf("Queue is empty")
}
```



General useage:

```c
void print(int* array, int front, int tail);
void enqueue(int* array, int* front, int* tail, int item);
int dequeue(int* array, int*front, int* tail);
```



API - library of functions - ממשק - stblib.h

bunch of functions for a certain data  type

איתחולתנאיקידום

לתזוזה על רשימה מקושרת נעשה TEMP ונרוץ עליו כדי לא לאבד את הנתונים שלנו

מקרה קצה:
התקבלה רשימה ריקה

מספר משתני עזר:מחיקה -

1. משתנה לרוץ על הרשימה
2. משתנה שקודם ל1



### Circular Queue

##### Introduction:

**Simple** : 

> Circular queue is basically going around in a loop. like a dog chasing its own tail.

**Advanced**: 

> Circular Queue works by the process of circular increment i.e. when we  try to increment any variable and we reach the end of the queue, we  start from the beginning of the queue by modulo division with the queue  size.

##### Important Key-Points:

> We must remember are working in a loop, thus:
>
> Enter element: R =  (R+1)%capacity 
>
> Remove element: F =  (F+1)%capacity
>
> <img src="C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\circular-increment.png" style="zoom:50%;" />
>
> Queue operations work as follows:
>
> - Two pointers called FRONT and REAR are used to keep track of the first and last elements in the queue.
> - When initializing the queue, we set the value of FRONT and REAR to -1.
> - On enqueuing an element, we circularly increase the value of REAR index and place the new element in the position pointed to by REAR.
> - On dequeuing an element, we return the value pointed to by FRONT and circularly increase the FRONT index.
> - Before enqueuing, we check if the queue is already full.
> - Before dequeuing, we check if the queue is already empty.
> - When enqueuing the first element, we set the value of FRONT to 0.
> - When dequeuing the last element, we reset the values of FRONT and REAR to -1.
>
> However, the check for full queue has a new additional case:
>
> - Case 1: FRONT = 0 && `REAR == SIZE - 1`
> - Case 2: `FRONT = REAR + 1`
>
> The second case happens when REAR starts from 0 due to circular increment and when its value is just 1 less than FRONT, the queue is full.
>
> <img src="C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\circular-queue-program.png" style="zoom:67%;" />
>
> ​                      
>
> 



##### Do's & Dont's

| ✅ Do this | ❌ Don't do this |
| :-------: | :-------------: |
|           |                 |

##### Example:

```c
#include <cstdlib>
#include <stdio.h> 
#include <stdlib.h>
#define MIN_QUEUE_SIZE  3 #define Error( Str ) {printf("%s\n", Str ); exit(1);}

typedef struct
{
	int Capacity; // Maximum queue size
	int Front, Rear, Size, *Array;
} QueueRecord;

typedef QueueRecord* Queue; //what is Queue?

char menu();
Queue createQueue(int MaxElements);
void init(Queue Q);
void enqueue(Queue Q, int x);
int dequeue(Queue Q);
int nextPlace(Queue Q, int Value);
void disposeQueue(Queue Q);
void printQueue(Queue Q);
int isEmpty(Queue Q);
int isFull(Queue Q);

char
menu()
{
	printf
		("\n\t MAIN MENU:\ \n 1. Add element \ \n 2. Delete element\ \n 3. Print queue \ \n 4. Clear queue \ \n 0. Exit\ \n Your choice: ");
	_flushall();
	return (getchar());
}

int
main()
{
	Queue Q = createQueue(6);
	int i;
	while (1)
	{
		switch (menu())
		{
		case '1':
			if (isFull(Q))
			{
				printf("The queue is Full!");
				system("cls");
				break;
			}
			printf("Enter the data: ");
			scanf("%d", &i);
			enqueue(Q, i);
			break;
		case '2':
			if (isEmpty(Q))
			{
				printf("The queue is Empty!");
				break;
			}
			printf("%d is dequeued", dequeue(Q));
			break;
		case '3':
			printf("Queue content:\n");
			printQueue(Q);
			break;
		case '4':
			printf("Clear queue:\n");
			while (!isEmpty(Q))
				printf("%d\n", dequeue(Q));
			break;
		case '0':
			disposeQueue(Q);
			printf("Exit...");
			return (0);
		default:
			printf("Wrong. Pls try again...");
		} // switch
	} // while 
} // main


Queue
createQueue(int MaxElements)
{
	Queue Q;
	if (MaxElements < MIN_QUEUE_SIZE)
	printf("Queue size is too small");
	Q = (Queue)malloc(sizeof(QueueRecord));
	if (!Q)
		printf("Out of space!!!");
	Q->Array = (int*)malloc(sizeof(int) * MaxElements);
	if (!Q->Array)
		printf("Out of space!!!");
	Q->Capacity = MaxElements;
	init(Q);
	return Q;
}


void
init(Queue Q)
{
	//Initialize the queue 
	Q->Size = 0;
	Q->Front = 1;
	Q->Rear = 0;
}

int
nextPlace(Queue Q, int Value)
{
	// Finding the place for enqueue/dequeue
	return ((Value + 1) % Q->Capacity); //Circular!! 
}

void
enqueue(Queue Q, int x)
{
	if (isFull(Q))
	{
		printf("enqueue: Queue is Full \n");
		return;
	}
	Q->Size++;
	Q->Rear = nextPlace(Q, Q->Rear);
	Q->Array[Q->Rear] = x;
}

int
dequeue(Queue Q)
{
	if (isEmpty(Q))
	{
		printf("dequeue:Queue is Empty \n");
		return 0;
	}
	int a = Q->Array[Q->Front];
	Q->Size--;
	Q->Front = nextPlace(Q, Q->Front);
	return (a);
}

int isEmpty(Queue Q) { return (!Q->Size); }
int isFull(Queue Q) { return (Q->Size == Q->Capacity); }

void disposeQueue(Queue Q)
{
	//Erase the queue
	if (Q)
	{
		free(Q->Array);
		free(Q);
	}
}

void printQueue(Queue Q)
{
	int i, k;
	for (k = 0, i = Q->Front; k < Q->Size; k++, i = (i + 1) % Q->Capacity)
		printf("Item %d is %d\n", k + 1, Q->Array[i]);
}

```

```c
// Circular Queue implementation in C 
// Code from the web

#include <stdio.h>

#define SIZE 5

int items[SIZE];
int front = -1, rear = -1;

// Check if the queue is full
int isFull() {
  if ((front == rear + 1) || (front == 0 && rear == SIZE - 1)) return 1;
  return 0;
}

// Check if the queue is empty
int isEmpty() {
  if (front == -1) return 1;
  return 0;
}

// Adding an element
void enQueue(int element) {
  if (isFull())
    printf("\n Queue is full!! \n");
  else {
    if (front == -1) front = 0;
    rear = (rear + 1) % SIZE;
    items[rear] = element;
    printf("\n Inserted -> %d", element);
  }
}

// Removing an element
int deQueue() {
  int element;
  if (isEmpty()) {
    printf("\n Queue is empty !! \n");
    return (-1);
  } else {
    element = items[front];
    if (front == rear) {
      front = -1;
      rear = -1;
    } 
    // Q has only one element, so we reset the 
    // queue after dequeing it. ?
    else {
      front = (front + 1) % SIZE;
    }
    printf("\n Deleted element -> %d \n", element);
    return (element);
  }
}

// Display the queue
void display() {
  int i;
  if (isEmpty())
    printf(" \n Empty Queue\n");
  else {
    printf("\n Front -> %d ", front);
    printf("\n Items -> ");
    for (i = front; i != rear; i = (i + 1) % SIZE) {
      printf("%d ", items[i]);
    }
    printf("%d ", items[i]);
    printf("\n Rear -> %d \n", rear);
  }
}

int main() {
  // Fails because front = -1
  deQueue();

  enQueue(1);
  enQueue(2);
  enQueue(3);
  enQueue(4);
  enQueue(5);

  // Fails to enqueue because front == 0 && rear == SIZE - 1
  enQueue(6);

  display();
  deQueue();

  display();

  enQueue(7);
  display();

  // Fails to enqueue because front == rear + 1
  enQueue(8);

  return 0;
}
```



##### Circular Queue Applications

- CPU scheduling
- Memory management
- Traffic Management



###  Abstract Data Structure

##### Introduction: 

**Simple** : 

> Abstract Data Structure is also called an ADT.

**Advanced**: 

> 

##### Important Key-Points:

> 

##### Do's & Dont's

| ✅ Do this | ❌ Don't do this |
| :-------: | :-------------: |
|           |                 |

##### Example:

```c
<code>
```











### Stack - מחסנית

##### Introduction:

**Simple** : 

> A stack is like a weapon magazine, last bullet is first to shoot, first bullet is last to shoot.
>
> It is an array that we work in the methology of "Last In, First Out" essencially like a gun. The system is called **"LIFO"**.
>
> We have 3 basic functions:
>
> - Push
>- Pop
> - Display
>
> <img src="C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\stack1.png" alt="stack" style="zoom: 67%;" />



**Advanced**: 

> ### Stack Specification
>
> A stack is an object or more specifically an abstract data structure(ADT) that allows the following operations:
>
> - `Push`: Add an element to the top of a stack
> - `Pop`: Remove an element from the top of a stack
> - `IsEmpty`: Check if the stack is empty
> - `IsFull`: Check if the stack is full
> - `Peek`: Get the value of the top element without removing it

##### **Important Key-Points:**

> ### How to Stack!
>
> 1. A pointer called TOP is used to keep track of the top element in the stack.
>
> 2. When initializing the stack, we set its value to -1 so that we can check if the stack is empty by comparing `TOP == -1`.
>
> 3. On pushing an element, we increase the value of TOP and place the new element in the position pointed to by TOP.
>
> 4. On popping an element, we return the element pointed to by TOP and reduce its value.
>
> 5. Before pushing, we check if the stack is already full
>
> 6. Before popping, we check if the stack is already empty
>
>    <img src="C:\Users\fello\Desktop\University\Programming\Snippets\Mark Text - Markdown backup\images\stack-operations.png" alt="stack" style="zoom:67%;" />

##### Do's & Dont's:

|                   ✅ Do this                   |           ❌ Don't do this           |
| :-------------------------------------------: | :---------------------------------: |
|    **Top** = top of the stack aka Last In     |     Bottom is the last in stack     |
| **Bottom** = bottom of the stack aka First In |      Top is the first in stack      |
|           **Empty Stack Size = -1**           |        Empty stack size = 0         |
|   We need the size of the **maximum** stack   | Don't know what is the maximum size |
|                                               |                                     |

##### Example:

```c
// Stack implementation in C

#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int count = 0;

// Creating a stack
struct stack {
  int items[MAX];
  int top;
};
typedef struct stack st;

void createEmptyStack(st *s) {
  s->top = -1;
}

// Check if the stack is full
int isfull(st *s) {
  if (s->top == MAX - 1)
    return 1;
  else
    return 0;
}

// Check if the stack is empty
int isempty(st *s) {
  if (s->top == -1)
    return 1;
  else
    return 0;
}

// Add elements into stack
void push(st *s, int newitem) {
  if (isfull(s)) {
    printf("STACK FULL");
  } else {
    s->top++;
    s->items[s->top] = newitem;
  }
  count++;
}

// Remove element from stack
void pop(st *s) {
  if (isempty(s)) {
    printf("\n STACK EMPTY \n");
  } else {
    printf("Item popped= %d", s->items[s->top]);
    s->top--;
  }
  count--;
  printf("\n");
}

// Print elements of stack
void printStack(st *s) {
  printf("Stack: ");
  for (int i = 0; i < count; i++) {
    printf("%d ", s->items[i]);
  }
  printf("\n");
}

// Driver code
int main() {
  int ch;
  st *s = (st *)malloc(sizeof(st));

  createEmptyStack(s);

  push(s, 1);
  push(s, 3);
  push(s, 4);

  printStack(s);

  pop(s);

  printf("\nAfter popping out\n");
  printStack(s);
}
```

##### Stack Applications

> Although stack is a simple data structure to implement, it is very powerful. The most common uses of a stack are:
>
> - **To reverse a word** - Put all the letters in a stack and pop them out. Because of the LIFO order of stack, you will get the  letters in reverse order.
> - **In compilers** - Compilers use the stack to calculate the value of expressions like `2 + 4 / 5 * (7 - 9)` by converting the expression to prefix or postfix form.
> - **In browsers** - The back button in a browser saves  all the URLs you have visited previously in a stack. Each time you visit a new page, it is added on top of the stack. When you press the back  button, the current URL is removed from the stack and the previous URL  is accessed.